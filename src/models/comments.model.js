// comments-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const comments = new Schema({
    text: { type: String, required: true },
    movieId: {type: Schema.Types.ObjectId, ref: 'movies'}
  }, {
    timestamps: true
  });

  return mongooseClient.model('comments', comments);
};
