const {BadRequest} = require('@feathersjs/errors');
// eslint-disable-next-line no-unused-vars
module.exports = function () {
  return async context => {
    if (typeof context.data.movieId !== 'undefined') {
      let movie = await context.app.service('movies').find(
        {
          query: {
            _id: context.data.movieId
          }
        });
      if (movie.data.length !== 0) {
        return context;
      }
      else {
        throw new BadRequest('The movie with provided id does not exist.');
      }
    }
    throw new BadRequest('movieId cannot be empty.');
  };
};
