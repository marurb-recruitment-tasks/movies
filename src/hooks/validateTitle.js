// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const isEmpty = require('validator/lib/isEmpty');
const {BadRequest} = require('@feathersjs/errors');
// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    if (context.data && typeof context.data.title !== 'undefined') {
      if (!isEmpty(context.data.title)) {
        return context;
      }
    }
    throw new BadRequest('Title cannot be empty');
  };
};
