const axios = require('axios');
const {BadRequest} = require('@feathersjs/errors');

module.exports = function () {
  return async context => {
    let { data } = await axios.get('http://omdbapi.com/', {
      params: {
        apikey: '23a8a436',
        t: context.data.title
      }
    });
    if (data.Response === 'True') {
      context.data.title = data.Title; // To replace the title with full correct match from Imdb
      context.data.year = data.Year;
      context.data.director = data.Director;
      context.data.imdbRating = data.imdbRating;
    }
    else {
      // We can assume that if the movie does not exist in Imdb, the title is incorrect.
      throw new BadRequest('Movie does not exist. Check the title and try again...');
    }
    return context;
  };
};
