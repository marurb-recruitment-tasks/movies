const movies = require('./movies/movies.service.js');
const comments = require('./comments/comments.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(movies);
  app.configure(comments);
};
