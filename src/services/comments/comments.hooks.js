const validateMovie = require('../../hooks/validateMovie');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [ validateMovie() ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
