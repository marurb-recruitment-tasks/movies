const validateTitle = require('../../hooks/validateTitle');
const fetchMovieData = require('../../hooks/fetchMovieData');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [ validateTitle(), fetchMovieData() ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
