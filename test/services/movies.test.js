const assert = require('assert');
const app = require('../../src/app');

describe('\'movies\' service', () => {
  it('registered the service', () => {
    const service = app.service('movies');

    assert.ok(service, 'Registered the service');
  });

  it('validates presence of the title in the request', async () => {
    try {
      await app.service('movies').create({});
      assert.fail('Should never get here');
    } catch(error) {
      assert.equal(error.name, 'BadRequest');
    }
  });

  it('if title present it creates the movie', async () => {
    const movieWithTitle = await app.service('movies').create({
      title: 'Gladiator'
    });
    assert.equal(movieWithTitle.title, 'Gladiator');
    app.service('movies').remove(movieWithTitle._id);
  });

  it('it is changing the title to full title from imdb', async () => {
    const movieWithTitle = await app.service('movies').create({
      title: 'inglorious'
    });
    assert.equal(movieWithTitle.title, 'The Inglorious Bastards');
    await app.service('movies').remove(movieWithTitle._id);
  });

  it('it is returning list of movies', async () => {
    let inglorious = await app.service('movies').create({
      title: 'inglorious'
    });

    let gladiator = await app.service('movies').create({
      title: 'Gladiator'
    });

    let movies = await app.service('movies').find();
    assert.ok(movies.data.length > 0);

    await app.service('movies').remove(inglorious._id);
    await app.service('movies').remove(gladiator._id);
  });
});
