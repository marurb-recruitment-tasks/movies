const assert = require('assert');
const app = require('../../src/app');

describe('\'comments\' service', () => {
  it('registered the service', () => {
    const service = app.service('comments');

    assert.ok(service, 'Registered the service');
  });

  it('validates presence of movieId in the request', async () => {
    try {
      await app.service('comments').create({});
      assert.fail('Should never get here');
    } catch(error) {
      assert.equal(error.name, 'BadRequest');
    }
  });

  it('validates if movieId is the id of existing movie in db', async () => {
    try {
      await app.service('comments').create({ movieId: '5ab95830d09ae23164e085a2'});
      assert.fail('Should never get here');
    } catch(error) {
      assert.equal(error.name, 'BadRequest');
    }
  });

  it('allows to create comment with correct data', async () => {
    let movie = await app.service('movies').create({title: 'Gladiator'});
    let comment = await app.service('comments').create({
      movieId: movie._id,
      text: 'Test'
    });
    assert.equal(comment.text, 'Test');
    assert.equal(comment.movieId, movie._id.toString());

    // clean up
    app.service('movies').remove(movie._id);
    app.service('comments').remove(comment._id);
  });

  it('returns list of all comments and filter comments with movieId', async () => {
    let movie1 = await app.service('movies').create({title: 'Gladiator'});
    let comment1 = await app.service('comments').create({
      movieId: movie1._id,
      text: 'Test 1'
    });
    let movie2 = await app.service('movies').create({title: 'Inglorious'});
    let comment2 = await app.service('comments').create({
      movieId: movie2._id,
      text: 'Test'
    });

    let movie2comments = await app.service('comments').find({
      query:
          {
            movieId: movie2._id
          }
    });
    let allComments = await app.service('comments').find();

    assert.ok(allComments.data.length >= 2);
    assert.equal(movie2comments.data.length, 1);

    // clean up
    app.service('movies').remove(movie1._id);
    app.service('comments').remove(comment1._id);
    app.service('movies').remove(movie2._id);
    app.service('comments').remove(comment2._id);
  });
});
