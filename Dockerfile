
FROM node:9.9.0-alpine

WORKDIR /home/feathers
COPY package.json package.json
COPY config/ config/
COPY public/ public/
COPY src/ src/
ENV NODE_ENV 'production'
ENV PORT '8080'
RUN npm install --production
CMD ["node", "src/index.js"]
EXPOSE 8080
