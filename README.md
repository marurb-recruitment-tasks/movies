# net-guru-movies

> Recrutation task from Netguru

## Public availability

The Movies API is available on [https://net-guru-movies-app.marurb.ovh](https://net-guru-movies-app.marurb.ovh)
## About

This project uses [Feathers](http://feathersjs.com).
The choice was made based on great functionality out of the box and also previous experience with Feathers.
Feathers provides great integration between backend and frontend (framework agnostic) which makes it great choice for API.

## Features

The API exposes 4 endpoints:

`POST /movies` - requires only `title` in the request body

`GET /movies`

`POST /comments` - requires `movieId` of movie in the DB and `text`

`GET /comments` - can be filtered by `movieId`: `GET /comments?movieId=...`

Sorting:
Sorting is available on both GET endpoints by using syntax: `/endpoint?$sort[fieldName]=` 1 for ASC, -1 for DESC

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. This app requires MongoDB running locally with standard port and with the `mongodb` host pointing to 127.0.0.1 or any available remote MongoDB server.
3. Install your dependencies

    ```
    cd path/to/net-guru-movies; npm install
    ```

4. Start your app

    ```
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

Tests cover all requirements from the task like validating presence of movie title etc.
Test cover service layer only as Express layer which is used by Feathers to expose service methods over REST API is already tested by Feathers.
